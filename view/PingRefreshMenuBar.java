/*
 * Copyright 2016 Farid Joubbi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package view;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

/**
 * This class represents methods for creating a menu bar and items for it.
 *
 * @author Farid Joubbi
 * @version 1.0
 * @since 0.9
 */
class PingRefreshMenuBar {
    private final MenuBar menuBar = new MenuBar();
    private final Menu menuName1;
    private final Menu menuName2;
    private final Menu menuName3;

    PingRefreshMenuBar(String menuName1, String menuName2, String menuName3) {
        this.menuName1 = new Menu(menuName1);
        this.menuName2 = new Menu(menuName2);
        this.menuName3 = new Menu(menuName3);
        
        menuBar.setStyle("-fx-background-color: #778899");
        menuBar.getMenus().addAll(this.menuName1, this.menuName2, this.menuName3);
    }
    
        MenuItem addMenuItem(String name, Menu menuName) {
        
        MenuItem newItem = new MenuItem(name);
        menuName.getItems().addAll(newItem);
        
        return newItem;
    }
    
    MenuItem addMenuSeparator(Menu menuName) {
        
        MenuItem newItem = new SeparatorMenuItem();
        menuName.getItems().addAll(newItem);
        
        return newItem;
    }
    
    MenuBar getMenuBar() {
        
        return menuBar;
    }
    
    Menu getMenuName1() {
       
        return menuName1;
    }
    
    Menu getMenuName2() {
        
        return menuName2;
    }
    
    Menu getMenuName3() {
        
        return menuName3;
    }
}
