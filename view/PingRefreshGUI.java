/*
 * Copyright 2016 Farid Joubbi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package view;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import pingrefresh.PingRefresh;

/**
 * This class is the graphical user interface to PingRefresh.
 *
 * @author Farid Joubbi
 * @version 1.1
 * @since 0.9
 */
public class PingRefreshGUI extends Application {

    private final PingRefresh pr = new PingRefresh();

    private TextArea ipAddressInput;
    private Button pingButton;

    public PingRefreshGUI() {
    }

    /**
     * Creates a VBox containing a text area for host addresses.
     *
     * @return a VBox containing a text area for host addresses.
     */
    private VBox addPingBox() {

        Label hostsToPingLabel = new Label("Hosts to ping");
        hostsToPingLabel.setFont(Font.font(null, FontWeight.BOLD, 16));
        hostsToPingLabel.setPadding(new Insets(10, 12, 15, 12));

        ipAddressInput = new TextArea();
        ipAddressInput.setPrefSize(400, 500);

        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);
        hbox.setStyle("-fx-background-color: #778899");

        pingButton = new Button("Ping");
        pingButton.setStyle("-fx-font-weight: bold;");
        pingButton.setPrefSize(100, 20);

        hbox.setPadding(new Insets(15, 0, 0, 300));
        hbox.getChildren().addAll(pingButton);

        VBox vbox = new VBox();
        vbox.setPadding(new Insets(15, 12, 15, 12));
        vbox.getChildren().addAll(hostsToPingLabel, ipAddressInput, hbox);

        return vbox;
    }

    @Override
    public void start(Stage primaryStage) {

        BorderPane border = new BorderPane();

        PingRefreshMenuBar menu = new PingRefreshMenuBar("File", "Convert", "Help");

        menu.addMenuItem("Open", menu.getMenuName1()).setOnAction((ActionEvent e) -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open host file");
            fileChooser.setInitialFileName("pingrefresh.txt");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Text files", "*.txt"));

            File selectedFile = fileChooser.showOpenDialog(null);

            if (selectedFile != null) {
                try {
                    readAddresses();
                    pr.OpenPingFile(selectedFile.getName());
                    updateAddressField();
                } catch (IOException fnfe) {
                    System.out.println("The file " + selectedFile.getName() + " could not be found.");
                }
            } else {
                System.out.println("File selection cancelled.");
            }
        });

        menu.addMenuItem("Save", menu.getMenuName1()).setOnAction((ActionEvent e) -> {
            readAddresses();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save");
            fileChooser.setInitialFileName("pingrefresh.txt");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Textfiles", "*.txt"));

            File selectedFile = fileChooser.showSaveDialog(null);

            if (selectedFile != null) {
                try {
                    pr.SavePingFile(selectedFile.getAbsolutePath());
                } catch (Exception error) {
                    System.out.println("Something went wrong saving the file!");
                }
            } else {
                System.out.println("File selection cancelled.");
            }
        });

        menu.addMenuSeparator(menu.getMenuName1());

        menu.addMenuItem("Exit", menu.getMenuName1()).setOnAction(event
                -> primaryStage.fireEvent(
                        new WindowEvent(
                                primaryStage,
                                WindowEvent.WINDOW_CLOSE_REQUEST
                        )
                )
        );

        menu.addMenuItem("To lowercase", menu.getMenuName2()).setOnAction((ActionEvent e) -> {
            pr.convertTolower();
            updateAddressField();
        });

        menu.addMenuItem("To IP address", menu.getMenuName2()).setOnAction((ActionEvent e) -> {

            pr.clearAddresses();
            readAddresses();

            try {
                pr.convertToIpAddress();
            } catch (UnknownHostException ex) {
                unknownHostAlert();
            }
            updateAddressField();
        });

        menu.addMenuItem("To FQDN", menu.getMenuName2()).setOnAction((ActionEvent e) -> {

            pr.clearAddresses();
            readAddresses();

            try {
                pr.convertToFqdn();
            } catch (UnknownHostException ex) {
                unknownHostAlert();
            }
            updateAddressField();
        });

        menu.addMenuItem("Sort", menu.getMenuName2()).setOnAction((ActionEvent e) -> {

            pr.clearAddresses();
            readAddresses();

            pr.sortAddresses();

            updateAddressField();
        });

        menu.addMenuItem("About", menu.getMenuName3()).setOnAction((ActionEvent e) -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("About PingRefresh");
            alert.setHeaderText(null);
            alert.setContentText("PingRefresh version 1.1\n"
                    + "\nLicensed under the Apache License, v2.0\n"
                    + "Written by Farid Joubbi\n"
                    + "https://gitlab.com/faridj/PingRefresh"
            );
            alert.showAndWait();
        });

        Text logo = new Text();
        logo.setX(10.0f);
        logo.setY(140.0f);
        logo.setCache(true);
        logo.setText("PingRefresh");
        logo.setFill(Color.RED);
        logo.setFont(Font.font(null, FontWeight.BOLD, 28));
        logo.setEffect(new GaussianBlur());

        VBox reachableBox = new VBox();
        reachableBox.setPadding(new Insets(15, 12, 15, 12));
        Label reachableLabel = new Label("Reachable hosts");
        reachableLabel.setFont(Font.font(null, FontWeight.BOLD, 16));
        reachableLabel.setPadding(new Insets(10, 12, 15, 12));
        TextArea reachableArea = new TextArea();
        reachableArea.setPrefSize(400, 500);
        reachableArea.setEditable(false);
        reachableBox.getChildren().addAll(reachableLabel, reachableArea);

        VBox unreachableBox = new VBox();
        unreachableBox.setPadding(new Insets(15, 12, 15, 12));
        Label unreachableLabel = new Label("Unreachable hosts");
        unreachableLabel.setFont(Font.font(null, FontWeight.BOLD, 16));
        unreachableLabel.setPadding(new Insets(10, 12, 15, 12));
        TextArea unreachableArea = new TextArea();
        unreachableArea.setPrefSize(400, 500);
        unreachableArea.setEditable(false);
        unreachableBox.getChildren().addAll(unreachableLabel, unreachableArea);

        HBox resultBox = new HBox();
        resultBox.getChildren().addAll(reachableBox, unreachableBox);
        border.setCenter(resultBox);
        border.setStyle("-fx-background-color: #778899");

        VBox pingBox = addPingBox();
        border.setLeft(pingBox);

        border.setBottom(logo);
        border.setTop(menu.getMenuBar());

        Scene scene = new Scene(border, 1200, 680);

        primaryStage.setTitle("PingRefresh");
        primaryStage.setScene(scene);
        primaryStage.show();
        ipAddressInput.requestFocus();

        pingButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                unreachableArea.clear();
                reachableArea.clear();
                pr.clearReachable();
                pr.clearUnreachable();
                pr.clearAddresses();

                readAddresses();

                pr.checkReachable();

                for (int i = 0; i < pr.getReachableSize(); i++) {
                    reachableArea.appendText(pr.getReachable().get(i) + "\n");
                }

                for (int i = 0; i < pr.getUnreachableSize(); i++) {
                    unreachableArea.appendText(pr.getUnreachable().get(i) + "\n");
                }

                pr.clearAddresses();
            }
        });

    }

    /**
     * Reads the addresses typed in or read from file.
     */
    private void readAddresses() {

        String ipAddress = ipAddressInput.getText();

        BufferedReader in = new BufferedReader(new StringReader(ipAddress));
        String str;
        try {
            while ((str = in.readLine()) != null) {
                pr.addAddress(str);
            }
        } catch (IOException ex) {
            Logger.getLogger(PingRefreshGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateAddressField() {

        ipAddressInput.clear();
        for (int i = 0; i < pr.getAddressesSize(); i++) {
            ipAddressInput.appendText(pr.getAddresses().get(i) + "\n");
        }
    }

    /**
     * Shows an alert if a host is unknown.
     */
    private void unknownHostAlert() {

        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Not a valid address");
        alert.setHeaderText("Not a valid address");
        alert.setContentText("The addres \'" + pr.getOffendingHost() + "\' is not valid!");
        alert.showAndWait();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        launch(args);
    }
}
