# PingRefresh

A small utility with a graphical user interface used to see if hosts in the network are responding. It is also possible to convert the hostnames into FQDN, IP address and lowercase.
The addresses to check can be typed in or read from a file.

PingRefresh is written in Java using JavaFX for the graphical components in the user interface.

Licensed under the Apache license version 2.0
Written by farid.joubbi@consign.se

## Screenshot
![Alt text](PingRefresh.png?raw=true "PingRefresh screenshot")

