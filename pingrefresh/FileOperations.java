/*
 * Copyright 2016 Farid Joubbi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pingrefresh;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Makes it possible to save and retrieve host addresses from a file.
 *
 * @author Farid Joubbi
 * @version 1.1
 * @since 0.9
 */
public class FileOperations {

    FileOperations() {
    }

    /**
     * Reads the contents of a selected file.
     *
     * @param filename the name of the file to read from
     * @return the addresses that were read from the file
     * @throws IOException
     */
    List<String> ReadFromFile(String filename) throws IOException {
        List<String> addresses = new ArrayList<>();
        Path file = Paths.get(filename);

        Charset charset = Charset.forName("US-ASCII");
        try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                addresses.add(line);
            }
        }

        return addresses;
    }

    /**
     * Saves addresses to a selected file.
     *
     * @param filename the name of the file to save to
     * @param listOfAddresses a list of all the addresses to save
     * @throws IOException
     */
    void saveToFile(String filename, List<String> listOfAddresses) throws IOException {
        Path out = Paths.get(filename);
        Files.write(out, listOfAddresses, Charset.defaultCharset());

        //System.out.println("Saved the file " + filename);
    }
}
