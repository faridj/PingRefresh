/*
 * Copyright 2016 Farid Joubbi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pingrefresh;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Contains the lists of addresses and the logic for those lists. This class is
 * the main control class of PingRefresh.
 *
 * @author Farid Joubbi
 * @version 1.1
 * @since 0.9
 */
public class PingRefresh {

    private List<String> listOfAddresses;
    private final List<String> listOfReachable;
    private final List<String> listOfUnreachable;
    private String offendingHost;

    public PingRefresh() {
        this.listOfAddresses = new ArrayList<>();
        this.listOfReachable = new ArrayList<>();
        this.listOfUnreachable = new ArrayList<>();
    }

    /**
     * Opens a file containing host addresses.
     *
     * @param filename the name of the file containing the addresses
     * @throws IOException
     */
    public void OpenPingFile(String filename) throws IOException {

        if (filename != null) {
            FileOperations fileOperations = new FileOperations();
            listOfAddresses.addAll(fileOperations.ReadFromFile(filename));
            cleanAddresses();
        } else {
            System.out.println("File selection cancelled.");
        }
    }

    /**
     * Saves host addresses to a file.
     *
     * @param filename the name of the file to save to
     */
    public void SavePingFile(String filename) {

        if (filename != null) {
            try {
                FileOperations fileOperations = new FileOperations();
                fileOperations.saveToFile(filename, listOfAddresses);
            } catch (Exception error) {
                System.out.println("Something went wrong saving the file!");
            }
        } else {
            System.out.println("File selection cancelled.");
        }
    }

    /**
     * Removes duplicates and empty "rows" from the address list.
     */
    private void cleanAddresses() {

        //Remove empty "rows" and spaces
        for (int i = 0; i < listOfAddresses.size(); i++) {
            if (listOfAddresses.get(i).isEmpty()) {
                listOfAddresses.remove(i);
            }
            else{
                listOfAddresses.set(i, listOfAddresses.get(i).replaceAll("\\s+",""));
            }
        }
        
        //Remove duplicates
        listOfAddresses = new ArrayList<>(new LinkedHashSet<>(listOfAddresses));        
    }

    /**
     * Does a ping to all the hosts to see if they are reachable.
     */
    public void checkReachable() {

        cleanAddresses();
        PingCommand c1 = new PingCommand();

        for (int i = 0; i < listOfAddresses.size(); i++) {
            try {
                if (c1.isReachable(listOfAddresses.get(i))) {
                    listOfReachable.add(listOfAddresses.get(i));
                } else {
                    listOfUnreachable.add(listOfAddresses.get(i));
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } catch (InterruptedException ex) {
                Logger.getLogger(PingRefresh.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //System.out.println("done");
    }

    /**
     * Converts the addresses to all lowercase.
     */
    public void convertTolower() {

        for (int i = 0; i < listOfAddresses.size(); i++) {
            listOfAddresses.set(i, listOfAddresses.get(i).toLowerCase());
        }
    }

    /**
     * Converts all the addresses to Fully Qualified Domain Names.
     *
     * @throws UnknownHostException
     */
    public void convertToFqdn() throws UnknownHostException {

        cleanAddresses();
        
        for (int i = 0; i < listOfAddresses.size(); i++) {
            offendingHost = listOfAddresses.get(i);
            InetAddress address = InetAddress.getByName(listOfAddresses.get(i));
            listOfAddresses.set(i, address.getCanonicalHostName());
        }
    }

    /**
     * converts all the addresses to IP addresses.
     *
     * @throws UnknownHostException
     */
    public void convertToIpAddress() throws UnknownHostException {

        cleanAddresses();
                
        for (int i = 0; i < listOfAddresses.size(); i++) {
            offendingHost = listOfAddresses.get(i);
            InetAddress address = InetAddress.getByName(listOfAddresses.get(i));
            listOfAddresses.set(i, address.getHostAddress());
        }
    }

    /**
     * Sorts all the addresses alphabetically.
     */
    public void sortAddresses() {
        
        cleanAddresses();
        
        Collections.sort(listOfAddresses);
    }

    /**
     * Adds an address to the list.
     *
     * @param address the address to be added
     */
    public void addAddress(String address) {
        if (address != null) {
            listOfAddresses.add(address);
        }
    }

    public void clearAddresses() {
        listOfAddresses.clear();
    }

    public void clearReachable() {
        listOfReachable.clear();
    }

    public void clearUnreachable() {
        listOfUnreachable.clear();
    }

    public List<String> getAddresses() {
        return new ArrayList<>(listOfAddresses);
    }

    public List<String> getReachable() {
        return new ArrayList<>(listOfReachable);
    }

    public List<String> getUnreachable() {
        return new ArrayList<>(listOfUnreachable);
    }

    public int getAddressesSize() {
        return listOfAddresses.size();
    }

    public int getReachableSize() {
        return listOfReachable.size();
    }

    public int getUnreachableSize() {
        return listOfUnreachable.size();
    }

    public String getOffendingHost() {

        return offendingHost;
    }
}
