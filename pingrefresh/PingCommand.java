/*
 * Copyright 2016 Farid Joubbi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pingrefresh;

import org.apache.commons.lang3.SystemUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains a command that uses the ping utility provided by the operating
 * system.
 *
 * @author Farid Joubbi
 * @version 1.0
 * @since 0.9
 */
class PingCommand {

    PingCommand() {
    }

    /**
     * Checks which OS is used before pinging an address.
     *
     * @param internetProtocolAddress The Internet protocol address to ping
     * @return <code>True</code> if the address is responsive,
     * <code>false</code> otherwise
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    boolean isReachable(String internetProtocolAddress) throws IOException, InterruptedException {

        List<String> command = new ArrayList<>();
        command.add("ping");

        if (SystemUtils.IS_OS_WINDOWS) {
            command.add("-n");
        } else if (SystemUtils.IS_OS_UNIX) {
            command.add("-c");
        } else {
            throw new UnsupportedOperationException("Unsupported operating system");
        }

        command.add("1");
        command.add(internetProtocolAddress);

        ProcessBuilder processBuilder = new ProcessBuilder(command);
        Process process = processBuilder.start();

        process.waitFor();

        if (process.exitValue() != 0) {
            return false;
        }

        return true;
    }
}
