/*
 * Copyright 2016 Farid Joubbi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pingrefresh;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A class used for testing the control and object of PingRefresh.
 *
 * @author Farid Joubbi
 * @version 0.9
 * @since 0.9
 */
public class PingRefreshTest {

    public static void main(String[] args) {
        PingRefresh pr = new PingRefresh();

        try {
            pr.OpenPingFile("pingthing3.txt");
        } catch (IOException ex) {
            Logger.getLogger(PingRefreshTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("All: " + pr.getAddresses());
        pr.convertTolower();
        System.out.println("All again: " + pr.getAddresses());
        pr.checkReachable();

        System.out.println("Unreachable: " + pr.getUnreachable());
        System.out.println("Reachable: " + pr.getReachable());

        pr.addAddress(null);
        System.out.println("All: " + pr.getAddresses());
        pr.addAddress("joubbi.se");
        System.out.println("All: " + pr.getAddresses());

        System.out.println("_-_-_-_");
        try {
            pr.convertToFqdn();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PingRefreshTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("All: " + pr.getAddresses());

        try {
            pr.convertToIpAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PingRefreshTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("All: " + pr.getAddresses());

    }

}
